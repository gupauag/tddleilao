package br.com.leilao;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Leilao {

    public ArrayList<Lance> lances;

    public Leilao() {
        lances = new ArrayList<>();
    }

    public ArrayList<Lance> getLaces() {
        return lances;
    }

    public void setLances(Lance lace) {
        this.lances.add(lace);
    }

    public ArrayList<Lance> adicionarNovoLance(Lance lance){
        if(validarLance(lance)){
            this.setLances(lance);
            return getLaces();
        }else return null;



//        if(validarLance(lance)){
//            this.setLances(lance);
//            return true;
//        }else {
//            return false;
//        }
    }

    public boolean validarLance(Lance lance2){
        for (Lance lanceObjeto : lances){
            if (lance2.getValorDoLance() <= lanceObjeto.getValorDoLance()) {
                throw new RuntimeException("O valor do lance não pode ser menor que de qualquer registro");
                //return false;
            }
        }
        return true;
    }

}
