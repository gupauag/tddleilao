package br.com.leilao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Leiloeiro {

    private String nome;
    private Leilao leilao;

    public Leiloeiro() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornaMaiorLance(){
        ArrayList<Lance> lances = leilao.getLaces();
        if(lances.isEmpty()) throw new RuntimeException("Lista Vazia!!!");
        Lance lance = lances.get(0);

        for(Lance lanceDoFor : lances){
            if(lance.getValorDoLance() < lanceDoFor.getValorDoLance()){
                lance = lanceDoFor;
            }
        }

        return lance;



//        Collections.sort (lances, new Comparator() {
//            public int compare(Object o1, Object o2) {
//                Lance p1 = (Lance) o1;
//                Lance p2 = (Lance) o2;
//                return p1.getValorDoLance() < p2.getValorDoLance() ? -1 :
//                        (p1.getValorDoLance() > p2.getValorDoLance() ? +1 : 0);
//
//            }
//        };
    }

}