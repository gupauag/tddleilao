package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTeste {

    private Usuario usuario;
    private Usuario usuario2;
    private Lance lance;
    private Lance lance2;
    private Leilao leilao;
    private Leiloeiro leiloeiro;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario2 = new Usuario();
        lance = new Lance();
        lance2 = new Lance();
        leilao = new Leilao();
        leiloeiro = new Leiloeiro();

        usuario.setId(1);
        usuario.setNome("Usuario 1");
        lance.setUsuario(usuario);
        lance.setValorDoLance(1000.00);

        usuario2.setId(2);
        usuario2.setNome("Usuario 2");
        lance2.setUsuario(usuario2);
        lance2.setValorDoLance(2000.00);
    }

    @Test
    public void testeRetornaMaiorLance(){

        leilao.adicionarNovoLance(lance);
        leilao.adicionarNovoLance(lance2);

        leiloeiro.setNome("Leilao 1");
        leiloeiro.setLeilao(leilao);

        Lance lanceMaior = leiloeiro.retornaMaiorLance();

        Assertions.assertSame(lance2, lanceMaior); // ele espera um retorno diferente de NULL;
    }

    @Test
    public void testeRetornoMaiorListaVazia(){
        leilao.adicionarNovoLance(lance);
        leilao.adicionarNovoLance(lance2);

        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    leiloeiro.retornaMaiorLance();
                });
    }

}
