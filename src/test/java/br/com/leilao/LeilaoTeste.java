package br.com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class LeilaoTeste {

    private Usuario usuario;
    private Usuario usuario2;
    private Lance lance;
    private Lance lance2;
    private Leilao leilao;

    @BeforeEach //antes de executar cada metodo ele cria uma calculadora. não preciso então criar o objeto sempre!
    public void setUp(){
        leilao = new Leilao();
    }


    @Test
    public void testeAdcionarNovoLance(){
        usuario = new Usuario(1,"Usuario 1");
        usuario2 = new Usuario(2, "Usuario 2");
        lance = new Lance(usuario, 1000.00);
        lance2 = new Lance(usuario2, 500.00);

        ArrayList<Lance> lances = new ArrayList<>();
        lances.add(lance);

        //boolean valida = leilao.adicionarNovoLance(lance);

        ArrayList<Lance> lancesRetorno = new ArrayList<>();
        lancesRetorno = leilao.adicionarNovoLance(lance);

        boolean validaLista = false;
        if(lances.get(0).getValorDoLance() == lancesRetorno.get(0).getValorDoLance()) validaLista = true;

        Assertions.assertTrue(validaLista);
        //Assertions.assertEquals(true, lancesRetorno.get(0)); // ele espera o retorno como true qnd adiciona o Lance;
    }

    @Test
    public void testeValidarLanceMaior(){
        usuario = new Usuario(1,"Usuario 1");
        usuario2 = new Usuario(2, "Usuario 2");
        lance = new Lance(usuario, 1000.00);
        lance2 = new Lance(usuario2, 2000.00);

        boolean adicionouLance;

        leilao.adicionarNovoLance(lance);

        //System.out.println(leilao.getLaces().get(0).getValorDoLance());
        //System.out.println(lance2.getValorDoLance());

        adicionouLance = leilao.validarLance(lance2);

        //System.out.println(adicionouLance);

        Assertions.assertEquals(true, adicionouLance); // ele espera o retorno como true qnd adiciona o Lance;

//        Assertions.assertThrows(RuntimeException.class,
//                () -> {
//                    leilao.validarLance(lance2);
//                });

    }
    @Test
    public void testeValidarLanceMenor(){
        usuario = new Usuario(1,"Usuario 1");
        usuario2 = new Usuario(2, "Usuario 2");
        lance = new Lance(usuario, 1000.00);
        lance2 = new Lance(usuario2, 500.00);

        boolean adicionouLance;

        leilao.adicionarNovoLance(lance);

        //System.out.println(leilao.getLaces().get(0).getValorDoLance());
        //System.out.println(lance2.getValorDoLance());

        //adicionouLance = leilao.validarLance(lance2);

        //System.out.println(adicionouLance);

        //Assertions.assertEquals(true, adicionouLance); // ele espera o retorno como true qnd adiciona o Lance;

        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    leilao.validarLance(lance2);
                });

    }

}
